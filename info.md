# **GIT BASICS**
1)   git add "filename" or <.>                   : adding a file to git

2)   git commit "filename" or -a "nothing" or -m : commiting it

3)   git branch                                  : lookup for branches

4)   git switch "branchname"                     : switch to another branch

5)   git merge "branchname"                      : merge current branch with selected

6)   git restore                                 : undo all actions to current commit

7)   git reset "commithash"                      : delete the commit, NOT changes in files

8)   git reset --hard "commithash"               : delete the commit, ANS changes in files

8.1) git reset --hard HEAD~1                     : one commit back

9)   git push

10)  git pull

11)  git checkout "filename"                     : reset file state (if was not commited)

12)  git revert HEAD~"number-of-commits-back"    : reverts number of commits
---


    How to undo changes in file (not commited):
        git checkout "filename"
---

    How to undo commit (and save changes in file):
        git reset "commithash"
---
    How to undo commit (and undo changes in file):
        git reset --hard "commithash"
---
    How to undo push (and undo changes in file):
	    1) git reset "commithash"</br>
	    2) git push origin "branchname" --force
---
## **ADD PULL REQUESTS**
1) fork repository (press fork)
2) add upstream repository:</br>
2.1) git remote add upstream "ssh-link"</br>
2.2) git fetch upstream</br>
2.2.1) switch to branch you need to update</br>
2.3) git merge upstream/"branchname"</br>
2.4) add some changes</br>
2.5) git push</br>
2.6) create pull request</br>
2.6) go to 2.2) and 2.3) and update merge

## **RESOLVE PULL REQUESTS CONFLICT**
1) git fetch "git@gitlab.com:Hencsat46/testiculi-kal.git" 'develop'
2) git checkout -b 'testiculi-kal-develop' FETCH_HEAD</br>
^^^ if was not pull requested yet

1) git switch testiculi-kal-develop (previously requested branch)
2) git pull "git@gitlab.com:Hencsat46/testiculi-kal.git" 'develop'</br> (previously requested branch)
^^^ if was pull requested already
3) git switch develop (original branch)
4) git merge testiculi-kal-develop
5) resolve conflict
6) git push
---